import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';

const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;


const styles = StyleSheet.create({
     container: {
          alignSelf:'center',
          justifyContent:'center',
          borderRadius: 10,
          flexDirection:'row'
     },
     title_button:{
          
          alignSelf:'center'
     }

})
export default styles;
