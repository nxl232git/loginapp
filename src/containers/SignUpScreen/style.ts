import React from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { StyleSheet, Dimensions } from "react-native";
import {Palette} from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex:1,
        height: Heights,
        width: Widths,
        justifyContent:'center',
    },
    
})
export default styles;
