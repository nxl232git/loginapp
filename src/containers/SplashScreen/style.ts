import React from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
     container: {
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: Palette.red
     },
     image_background: {
          width: '90%',
          resizeMode: 'contain',
          margin: 30
     },
     activity_Indicator: {
          alignItems: 'center',
          height: 80,
     },
     title_splash:{
          fontSize: 30,
          fontWeight: 'bold',
          color: Palette.white
     }

})
export default styles;
