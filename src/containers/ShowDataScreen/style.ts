import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignSelf:'center'
    },
    text:{
        fontSize: 30
    }
    

})
export default styles;
